package com.example.TaskManagerAPI.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.TaskManagerAPI.dto.TaskDTO;
import com.example.TaskManagerAPI.entity.Task;
import com.example.TaskManagerAPI.exception.ErrorCode;
import com.example.TaskManagerAPI.exception.TaskManagerException;
import com.example.TaskManagerAPI.repository.TaskRepository;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	private final TaskRepository taskRepo;

	public TaskServiceImpl(TaskRepository taskRepository) {
		this.taskRepo = taskRepository;
	}

	@Override
	public List<TaskDTO> getAllTasks() throws TaskManagerException {
		List<Task> tasks = taskRepo.findAll();
		if (tasks.isEmpty()) {
			throw new TaskManagerException(ErrorCode.TASKS_NOT_FOUND);
		}
		return tasks.stream().map(TaskDTO::toDTO).collect(Collectors.toList());
	}

	@Override
	public TaskDTO getTaskById(Long id) throws TaskManagerException {
		Task task = taskRepo.findById(id).orElseThrow(() -> new TaskManagerException(ErrorCode.TASK_NOT_FOUND));
		return TaskDTO.toDTO(task);
	}
	
	@Override
	public TaskDTO createTask(TaskDTO taskDTO) {
		 Task task = new Task(taskDTO.getTitle(), taskDTO.getDescription(), taskDTO.isCompleted());
		    taskRepo.save(task);
		    return TaskDTO.toDTO(task);
	}
	
	
	@Override
	public TaskDTO updateTask(Long id, TaskDTO taskDTO) throws TaskManagerException {
	    Task task = taskRepo.findById(id)
	            .orElseThrow(() -> new TaskManagerException(ErrorCode.TASK_NOT_FOUND));
	    task.setTitle(taskDTO.getTitle());
	    task.setDescription(taskDTO.getDescription());
	    task.setCompleted(taskDTO.isCompleted());
	    taskRepo.save(task);
	    return TaskDTO.toDTO(task);
	}
	
	@Override
	public TaskDTO saveTask(TaskDTO taskDTO) {
		Task savedTask = taskRepo.save(taskDTO.toEntity());
		return TaskDTO.toDTO(savedTask);
	}

	@Override
	public Boolean deleteTask(Long id) throws TaskManagerException {
		if (!taskRepo.existsById(id)) {
			throw new TaskManagerException(ErrorCode.TASK_NOT_FOUND);
		}
		taskRepo.deleteById(id);
		return !taskRepo.existsById(id);
	}
}
