package com.example.TaskManagerAPI.service;

import java.util.List;

import com.example.TaskManagerAPI.dto.TaskDTO;

public interface TaskService {
	List<TaskDTO> getAllTasks();
	TaskDTO getTaskById(Long id);
	TaskDTO createTask(TaskDTO taskDTO);
	TaskDTO updateTask(Long id, TaskDTO taskDTO);
    TaskDTO saveTask(TaskDTO taskDTO);
    Boolean deleteTask(Long id);
}
