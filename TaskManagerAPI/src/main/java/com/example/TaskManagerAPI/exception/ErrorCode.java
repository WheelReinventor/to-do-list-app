package com.example.TaskManagerAPI.exception;

public enum ErrorCode {
	TASK_NOT_FOUND("Task not found."),
	TASKS_NOT_FOUND("Tasks not found."),
	INVALID_TASK("Invalid task details.");

	private final String message;

	ErrorCode(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
