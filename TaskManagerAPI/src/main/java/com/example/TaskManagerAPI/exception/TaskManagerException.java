package com.example.TaskManagerAPI.exception;

public class TaskManagerException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private final ErrorCode errorCode;

	public TaskManagerException(ErrorCode errorCode) {
		super(errorCode.getMessage());
		this.errorCode = errorCode;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}
}
