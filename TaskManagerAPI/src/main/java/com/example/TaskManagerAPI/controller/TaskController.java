package com.example.TaskManagerAPI.controller;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.TaskManagerAPI.dto.TaskDTO;
import com.example.TaskManagerAPI.exception.TaskManagerException;
import com.example.TaskManagerAPI.service.TaskService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("tasks")
public class TaskController {

	@Autowired
	private final TaskService taskService;

	public TaskController(TaskService taskService) {
		this.taskService = taskService;
	}

	@GetMapping("")
	public ResponseEntity<List<TaskDTO>> getAllTasks() {
		List<TaskDTO> tasksDTO = taskService.getAllTasks();
		return new ResponseEntity<List<TaskDTO>>(tasksDTO, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<TaskDTO> getTaskById(@PathVariable Long id) {
		TaskDTO taskDTO = taskService.getTaskById(id);
		return new ResponseEntity<TaskDTO>(taskDTO, HttpStatus.OK);
	}

	@PostMapping("")
	public ResponseEntity<TaskDTO> createTask(@RequestBody TaskDTO taskDTO) {
		taskDTO.setId(null); // set ID to null to let JPA generate the ID
		TaskDTO createdTaskDTO = taskService.createTask(taskDTO);
		return new ResponseEntity<TaskDTO>(createdTaskDTO, HttpStatus.CREATED); 
	}

	@ResponseBody
	@PutMapping("/{id}")
	public TaskDTO updateTask(@PathVariable Long id, @RequestBody TaskDTO taskDTO) {
		return taskService.updateTask(id, taskDTO);
	}

	@DeleteMapping("/{id}")
	public boolean deleteTask(@PathVariable Long id) {
		return taskService.deleteTask(id);
	}

	@ExceptionHandler(TaskManagerException.class)
	public ResponseEntity<Object> handleTaskNotFoundException(TaskManagerException e) {
		Map<String, Object> responseBody = new LinkedHashMap<>();
		responseBody.put("status", HttpStatus.NOT_FOUND.value());
		responseBody.put("message", e.getMessage());
		responseBody.put("timestamp", LocalDateTime.now());

		return new ResponseEntity<>(responseBody, HttpStatus.NOT_FOUND);
	}

}
