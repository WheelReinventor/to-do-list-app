package com.example.TaskManagerAPI.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.TaskManagerAPI.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

}
