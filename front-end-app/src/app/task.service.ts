import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from './task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private baseUrl = 'http://localhost:8080/tasks';

  constructor(private http: HttpClient) { }

  getAllTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(`${this.baseUrl}`);
  }

  addTask(task: Task): Observable<Task> {
    return this.http.post<Task>(`${this.baseUrl}`, task);
  }

  updateTask(id?: number, task?: Task): Observable<Task> {
    const url = `${this.baseUrl}/${id}`;
    return this.http.put<Task>(url, task);
  }

  deleteTask(taskId: number): Observable<boolean> {
    const url = `${this.baseUrl}/${taskId}`;
    return this.http.delete<boolean>(url);
  }

}
