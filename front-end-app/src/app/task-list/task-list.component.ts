import { Component, OnInit } from '@angular/core';
import { Task } from '../task';
import { TaskService } from '../task.service';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  tasks: Task[] = [];
  showAddForm = false;
  addedTask = {
    title: '',
    description: '',
    completed: false
  }

  originalTasks: Array<Task | null> = [];

  constructor(private taskService: TaskService) { }

  ngOnInit(): void {
    this.getTasks();
  }

  getTasks(): void {
    this.taskService.getAllTasks()
      .subscribe(tasks => this.tasks = tasks);
  }

  addNewTask() {
    if (this.addedTask.title && this.addedTask.description) {
      const newTask: Task = {
        title: this.addedTask.title,
        description: this.addedTask.description,
        completed: false,
        editing: false
      };
      this.taskService.addTask(newTask).subscribe((task: Task) => {
        this.tasks.push(task);
        this.addedTask = {
          title: '',
          description: '',
          completed: false
        };
        this.showAddForm = false;
      });
    }
  }

  editTask(task: Task) {
    const index = this.tasks.findIndex(t => t.id === task.id);
    if (index >= 0) {
      this.originalTasks[index] = { ...this.tasks[index] };
      this.tasks[index].editing = true;
    }
  }

  updateTask(task: Task) {
    this.taskService.updateTask(task.id, task)
      .subscribe(updatedTask => {
        const index = this.tasks.findIndex(t => t.id === updatedTask.id);
        if (index >= 0) {
          this.tasks[index] = updatedTask;
          this.tasks[index].editing = false;
        }
      });
  }

  cancelEdit(task: Task) {
    const index = this.tasks.findIndex(t => t.id === task.id);
    if (index >= 0) {
      this.tasks[index] = this.originalTasks[index] ?? { id: undefined, title: '', description: '', completed: false, editing: false };
      this.tasks[index].editing = false;
      this.originalTasks[index] = null;
    }
  }

  deleteTask(id?: number) {
    if (id) {
      this.taskService.deleteTask(id).subscribe(response => {
        if (response) {
          const index = this.tasks.findIndex(task => task.id === id);
          if (index >= 0) {
            this.tasks.splice(index, 1);
          }
        }
      });
    }
  }

  toggleAddForm() {
    this.showAddForm = !this.showAddForm;
    this.addedTask = { title: '', description: '', completed: false };
  }

}
